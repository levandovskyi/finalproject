package levan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter URL (without https//):");
        String userInput = s.nextLine();
        URL wiki = new URL("https://" + userInput + "/");
        InputStream stream = wiki.openStream();
        BufferedReader br = null;
        HashMap<Character, Integer> map = new HashMap<>();
        HashMap<Character, Integer> sorted;

        try {
            br = new BufferedReader(new InputStreamReader(stream));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
            }
            System.out.println("Number of symbols in the given array is: " + sb.length());

            char[] array = sb.toString().toCharArray();

            for (char c : array) {
                map.merge(c, 1, (a, b) -> a + b);
            }

            System.out.println("Unsorted map of all characters on the page: ");

            for (Map.Entry<Character, Integer> key : map.entrySet()) {
                System.out.println(key);
            }

            //sorts map
            sorted = sort(map);

            System.out.println();
            System.out.println("10 most used characters on a page:");
            System.out.println();

            //prints out 10 most used characters found in the HTML
            if (sorted.size() < 10) {
                for (int i = sorted.size() - 1; i > 0; i--) {
                    System.out.println(sorted.entrySet().toArray()[i] + " times");
                }
            }

            for (int i = sorted.size() - 1; i >= sorted.size() - 10; i--) {
                System.out.println(sorted.entrySet().toArray()[i] + " times");
            }



        } finally {
            if (br == null) {
                br.close();
            }
        }
    }


    //Map sorting algorithm
     private static LinkedHashMap<Character, Integer> sort(
             HashMap<Character, Integer> map) {
        List<Character> keys = new ArrayList<>(map.keySet());
        List<Integer> values = new ArrayList<>(map.values());

        //sorts keys and values in two different lists.
        Collections.sort(values);
        Collections.sort(keys);

        //map that will hold sorted map
        LinkedHashMap<Character, Integer> sorted =
                new LinkedHashMap<>();


        for (Integer value : values) {
            //iterator for key list
            Iterator<Character> iterator = keys.iterator();

            //while there are elements in the list
            while (iterator.hasNext()) {
                Character key = iterator.next();
                Integer comp = map.get(key);

                if (value.equals(comp)) {
                    iterator.remove();
                    sorted.put(key, value);
                    break;
                }
            }
        }
        return sorted;
    }

}
